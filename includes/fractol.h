#ifndef FRACTOL_H
# define FRACTOL_H
# include <stdlib.h>
# include <mlx.h>
# include <math.h>
# include <pthread.h>
# include <libft.h>

# include <stdio.h>

# define WINDOW_WIDTH 1000
# define WINDOW_HEIGHT 800
# define IMAGE_WIDTH 800
# define IMAGE_HEIGHT 800

# define COLOR_SELECT 0xA1AAAAFF
# define COLOR_MARKER 0xDAA88800
# define COLOR_MARKER_CENTER 0x00FFFFFF
# define SELECT_BORDER_SIZE 1
# define MARKER_THICKNESS 5

# define THREAD_NR 8

# define FRACTAL1 "julia"
# define FRACTAL2 "mandelbrot"
# define FRACTAL3 "burningship"

typedef struct	s_argb
{
	unsigned char	a;
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;
}				t_argb;

typedef struct	s_hsv
{
	double		h;
	double		s;
	double		v;
}				t_hsv;

typedef struct	s_complex
{
	double		r;
	double		i;
}				t_complex;

typedef struct	s_image
{
	void	*p_image;
	int		*p_pixel;
	int		line_size;
	int		bpp;
	int		endi;
	int		width;
	int		height;
}				t_image;

typedef struct	s_mouse
{
	int			x;
	int			y;
	int			mouse1;
	int			mouse2;
	int			wheel;
}				t_mouse;

typedef struct	s_select
{
	int			start_x;
	int			start_y;
	int			end_x;
	int			end_y;
}				t_select;

typedef struct	s_fractal
{
	int			max_iterations; // max nr of iterations
	t_complex	p; // image scale and center offset
	t_complex	n; // new value for complex nr
	t_complex	o; // old value for complex nr
	double		move_x; // x offset on image
	double		move_y; // y offset on image
	double		zoom; // zoom level
	double		*p_number; // the pointer for values for every pixel
	double		*offset_j; // offset for julia
}				t_fractal;

typedef struct	s_data_main
{
	void		*mlx;
	int			fractal_nr;
	char		*fractals[3];
	void		*(*fractal_solve[3])(void*);

	// WINDOW AND IMAGE SIZE
	double		proportion;
	int			win_width;
	int			win_height;
	int			img_width;
	int			img_height;
}				t_data_main;

typedef struct	s_thread
{
	int			id;
	int			y_start;
	int			y_end;
	t_fractal	*fractal; // fractal data
	t_data_main	*data_main; // not very clean ...
}				t_thread;

typedef struct	s_data
{
	void		*mlx;
	void		*win;
	t_image		*image; // image for fractal
	t_image		*image_s; // image for selection
	t_mouse		*mouse; // mouse data
	t_select	*select; // select data
	t_thread	*thread; // tread data (array with data for every thread - id gives fraction of creeen that will be rendered)
	void		*(*fractal_solve)(void*);
	int			is_fractal_changed;
	int			is_select_changed;
	int			is_render_required; // if true, render all active images
	t_data_main	*data_main; // not very clean ...
}				t_data;

t_image			*image_init(void *mlx, int w, int h);
t_thread		*thread_init(int nr_of_treads, int max_iterations, t_data_main *data_main);
t_fractal		*fractal_init(int thread_nr, int max_iterations, t_data_main *data_main);
void			*fractal_solve_mandelbrot(void *data_void);
int				frame_build(t_data *data);
void			image_clear(t_image *image, int opacity);
int				render(t_data *data);
int				hsv_to_hexa(t_hsv hsv);
void			start_fractal(char *fractal_name, t_data_main *data_main, int i);
#endif
