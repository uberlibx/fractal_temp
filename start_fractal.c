#include "fractol.h"

/*
 * Opens a new window to draw the chosen fractal
*/

void		start_fractal(char *fractal_name, t_data_main *data_main, int index)
{
	t_data		data;
	double		j_offset = 0;


	data_main->proportion = 1;
	if (data_main->fractal_nr == 2)
		data_main->proportion = 0.8;
	if (data_main->fractal_nr == 3)
		data_main->proportion = 0.6;

	data_main->win_width = WINDOW_WIDTH * data_main->proportion;
	data_main->win_height = WINDOW_HEIGHT * data_main->proportion;
	data_main->img_width = IMAGE_WIDTH * data_main->proportion;
	data_main->img_height = IMAGE_HEIGHT * data_main->proportion;



	data.win = mlx_new_window(data_main->mlx, data_main->win_width, data_main->win_height, ft_strtoupper(fractal_name));
	data.image = image_init(data_main->mlx, data_main->img_width, data_main->img_height);
	data.image_s = image_init(data_main->mlx, data_main->img_width, data_main->img_height);
	data.thread = thread_init(THREAD_NR, 256, data_main);
	//data.fractal_solve = fractal_solve_mandelbrot;
	data.fractal_solve = data_main->fractal_solve[index];
	//data.mouse = mouse_init();
	//data.select = select_init(data.image_s);
	data.is_fractal_changed = 1;
	data.is_select_changed = 0;
	data.is_render_required = 1;
	data.data_main = data_main;
	frame_build(&data); // To prevent initial blank screen, before first frame renders
	//mlx_loop_hook(data.mlx, frame_build, &data);
	//mlx_hook(data.win, 2, 1, event_keypress, &data);
	//mlx_hook(data.win, 4, 0, event_mouse_down, &data);
	//mlx_hook(data.win, 5, 0, event_mouse_up, &data);
	//mlx_hook(data.win, 6, 0, event_mouse_move, &data);
}
