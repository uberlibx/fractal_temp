#include "fractol.h"

void		*fractal_solve_mandelbrot(void *data_void)
{
	t_thread	*data;
	int		x;
	int		y;
	int		y_fractal;
	int		i;

	data = (t_thread*)data_void;
	y = -1 + data->y_start;
	y_fractal = -1;
	while (++y < data->y_end)
	{
		++y_fractal;
		x = -1;
		while (++x < data->data_main->img_width)
		{
			data->fractal->p.r = 1.5 * (x - data->data_main->img_width / 2) /
			(0.5 * data->fractal->zoom * data->data_main->img_width) + data->fractal->move_x;
    		data->fractal->p.i = (y - data->data_main->img_height / 2) /
			(0.5 * data->fractal->zoom * data->data_main->img_height) + data->fractal->move_y;
    		data->fractal->n.r = 0;
			data->fractal->n.i = 0;
			data->fractal->o.r = 0;
			data->fractal->o.i = 0;

			i = -1;
			while (++i < data->fractal->max_iterations)
    		{
      			data->fractal->o.r = data->fractal->n.r;
      			data->fractal->o.i = data->fractal->n.i;

				data->fractal->n.r = data->fractal->o.r * data->fractal->o.r -
				data->fractal->o.i * data->fractal->o.i + data->fractal->p.r;

      			data->fractal->n.i = 2 * data->fractal->o.r * data->fractal->o.i
				+ data->fractal->p.i;

      			if (data->fractal->o.r == data->fractal->n.r &&
					data->fractal->o.i == data->fractal->n.i)
      			{
      				i = data->fractal->max_iterations;
      				break;
      			}

      			if ((data->fractal->n.r * data->fractal->n.r +
					data->fractal->n.i * data->fractal->n.i) >= 1 << 8)
      				break;
    		}
    		data->fractal->p_number[x + y_fractal * data->data_main->img_width] = i;
		}
	}
	//printf("thread id: %i	y_start: %i	y_end: %i\n", data->id, data->y_start, data->y_end);
	return (NULL);
}
