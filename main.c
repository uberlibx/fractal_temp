#include "fractol.h"

static int	validate_input(int ac, char **av, t_data_main *data_main)
{
	while (--ac > 0)
	{
		if (ft_strcmp(av[ac], FRACTAL1) == 0)
		{
			data_main->fractal_nr++;
			data_main->fractals[ac - 1] = ft_strdup(FRACTAL1);
			data_main->fractal_solve[ac - 1] = fractal_solve_mandelbrot;
		}
		else if (ft_strcmp(av[ac], FRACTAL2) == 0)
		{
			data_main->fractal_nr++;
			data_main->fractals[ac - 1] = ft_strdup(FRACTAL2);
			data_main->fractal_solve[ac - 1] = fractal_solve_mandelbrot;
		}
		else if (ft_strcmp(av[ac], FRACTAL3) == 0)
		{
			data_main->fractal_nr++;
			data_main->fractals[ac - 1] = ft_strdup(FRACTAL3);
			data_main->fractal_solve[ac - 1] = fractal_solve_mandelbrot;
		}
	}
	return (data_main->fractal_nr);
}

static void	start_fractal_list(t_data_main *data_main)
{
	int		i;

	i = -1;
	while (++i < data_main->fractal_nr)
		start_fractal(data_main->fractals[i], data_main, i);
}

/*
 * [TODO] data_init.c
 * [TODO] fractal selector (command input, menu)
*/
int		main(int ac, char **av)
{
	t_data		data;
	double		j_offset = 0;
	void		*mlx;
	t_data_main	data_main;

	data_main.mlx = mlx_init();
	data_main.fractal_nr = 0;
	if (validate_input(ac, av, &data_main) == 0)
	{
		ft_putendl("No valid fractal detected / usage ?");
		return (0);
	}
	start_fractal_list(&data_main);
	mlx_loop(data_main.mlx);
/*
	data.mlx = mlx_init();


	data.win = mlx_new_window(data.mlx, WINDOW_WIDTH, WINDOW_HEIGHT, "Fract'ol");
	data.image = image_init(data.mlx, IMAGE_WIDTH, IMAGE_HEIGHT);
	data.image_s = image_init(data.mlx, IMAGE_WIDTH, IMAGE_HEIGHT);
	data.thread = thread_init(THREAD_NR, 256);
	data.fractal_solve = fractal_solve_mandelbrot;
	//data.mouse = mouse_init();
	//data.select = select_init(data.image_s);
	data.is_fractal_changed = 1;
	data.is_select_changed = 0;
	data.is_render_required = 1;
	frame_build(&data); // To prevent initial blank screen, before first frame renders
	mlx_loop_hook(data.mlx, frame_build, &data);
	//mlx_hook(data.win, 2, 1, event_keypress, &data);
	//mlx_hook(data.win, 4, 0, event_mouse_down, &data);
	//mlx_hook(data.win, 5, 0, event_mouse_up, &data);
	//mlx_hook(data.win, 6, 0, event_mouse_move, &data);


	mlx_loop(data.mlx);
	*/


	return (0);
}
