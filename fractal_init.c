#include "fractol.h"

t_fractal	*fractal_init(int nr_of_treads, int max_iterations, t_data_main *data_main)
{
	t_fractal	*fractal;
	int			i;

	fractal = (t_fractal*)malloc(sizeof(t_fractal));
	fractal->max_iterations = max_iterations;
	fractal->move_x = 0;
	fractal->move_y = 0;
	fractal->zoom = 1;
	fractal->p_number = (double*)malloc(sizeof(double) * data_main->img_width *
										data_main->img_height / nr_of_treads);
	printf("ok ok \n");
	return (fractal);
}
