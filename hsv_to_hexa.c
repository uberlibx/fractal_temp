#include "fractol.h"

int		hsv_to_hexa(t_hsv hsv)
{
	int		color;
	double	r, g, b, h, s, v; // This function works with floats between 0 and 1
	float	f, p, q, t;
	int		i;

    h = hsv.h / 256.0;
    s = hsv.s / 256.0;
    v = hsv.v / 256.0;

    // If saturation is 0, the color is a shade of gray
    if(s == 0)
    	r = g = b = v;

    // If saturation > 0, more complex calculations are needed
    // The HSV model can be presented on a cone with hexagonal shape. For each of the sides of the hexagon, a separate case is calculated:
    else
    {
        h *= 6; // To bring hue to a number between 0 and 6, better for the calculations
        i = (int)(floor(h));  // E.g. 2.7 becomes 2 and 3.01 becomes 3 or 4.9999 becomes 4
        f = h - i;  // The fractional part of h
        p = v * (1 - s);
        q = v * (1 - (s * f));
        t = v * (1 - (s * (1 - f)));
        switch(i)
        {
            case 0: r = v; g = t; b = p; break;
            case 1: r = q; g = v; b = p; break;
            case 2: r = p; g = v; b = t; break;
            case 3: r = p; g = q; b = v; break;
            case 4: r = t; g = p; b = v; break;
            case 5: r = v; g = p; b = q; break;
        }
    }

    color = (1 << 24) + ((int)(r * 255) << 16) + ((int)(g * 255) << 8) + ((int)(b * 255) | 0);

	return (color);
}
