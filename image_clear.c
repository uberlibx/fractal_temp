#include "fractol.h"

void	image_clear(t_image *image, int opacity)
{
	int		i;
	int		size;

	size = image->width * image->height;
	i = -1;
	while (++i < size)
	{
		if (opacity == 1)
			*(image->p_pixel + i) = 0X00000000;
		else
			*(image->p_pixel + i) = 0XFF000000;
	}
}
