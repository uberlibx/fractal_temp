#include "fractol.h"

t_thread	*thread_init(int nr_of_treads, int max_iterations, t_data_main *data_main)
{
	t_thread	*thread;
	int			i;

	thread = (t_thread*)malloc(sizeof(t_thread) * nr_of_treads);
	printf("ok\n");
	i = -1;
	while (++i < nr_of_treads)
	{
		thread[i].id = i;
		thread[i].y_start = i * data_main->img_width / THREAD_NR;
		thread[i].y_end = (i + 1) * data_main->img_height / THREAD_NR;
		thread[i].fractal = fractal_init(nr_of_treads, max_iterations, data_main);
		thread[i].data_main = data_main;
	}
	return (thread);
}
