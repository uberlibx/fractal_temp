#include "fractol.h"

int		frame_build(t_data *data)
{
	pthread_t	id[THREAD_NR];
	int			i;

	if (data->is_fractal_changed)
	{
		i = -1;
		while (++i < THREAD_NR)
			pthread_create(&id[i], NULL, fractal_solve_mandelbrot, &data->thread[i]);
		while (--i >= 0)
			pthread_join(id[i], NULL);
		//fractal_build(data);
		data->is_fractal_changed = 0;
		data->is_render_required = 1;
	}
	if (data->is_select_changed)
	{
		//select_build(data);
		data->is_render_required = 1;
	}
	render(data);
	return (0);
}
