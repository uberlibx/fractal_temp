NAME = fractol

#MLX_PATH = ./minilibx
MLX_PATH = /usr/local/

LIBFT_PATH = ./libft

#INCLUDES = -I $(MLX_PATH)
INCLUDES = -I $(MLX_PATH)/include
INCLUDES += -I $(LIBFT_PATH)/includes

#LIBS = -lpthread -L $(MLX_PATH) -lmlx -L $(LIBFT_PATH) -lmlx
LIBS =  -lpthread -L $(MLX_PATH)/lib -lmlx
LIBS += -L $(LIBFT_PATH) -lft

SRC_PATH = ./

SRC = 	fractal_init.c \
		fractal_solve_mandelbrot.c \
		frame_build.c \
		hsv_to_hexa.c \
		image_clear.c \
		image_init.c \
		main.c \
		render.c \
		thread_init.c \
		start_fractal.c \

OBJ = $(SRC:.c=.o)

FLAGS = -Wall -Werror -Wextra $(INCLUDES)
FLAGS = $(INCLUDES) -I./includes

FRMW = -framework OpenGL -framework AppKit

all:
#	@make -C ./libft
	@gcc $(FLAGS) -c $(SRC)
	@gcc $(FLAGS) $(LIBS) $(FRMW) $(OBJ) -o $(NAME)

clean:
#	@make clean -C ./libft
	@rm -f $(OBJ)

fclean:	clean
#	@make fclean -C ./libft
	@rm -f $(NAME)

re:	fclean all
