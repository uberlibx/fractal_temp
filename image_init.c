#include "fractol.h"

t_image		*image_init(void *mlx, int w, int h)
{
	t_image		*image;

	image = (t_image*)malloc(sizeof(t_image));
	image->width = w;
	image->height = h;
	image->bpp = 32;
	image->endi = 0;
	image->line_size = w * image->bpp;
	image->p_image = mlx_new_image(mlx, w, h);
	image->p_pixel = (int*)mlx_get_data_addr(image->p_image, &image->bpp,
										&image->line_size, &image->endi);
	return (image);
}
