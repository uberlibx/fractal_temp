#include "fractol.h"

static void		render_fractal(t_thread *thread, t_image *image)
{
	int		i;
	int		x;
	int		y;
	int		y_fractal;
	float	index_offset;
	t_hsv	color_hsv;
	int		t;

	index_offset = (1.0 / (float)THREAD_NR) * (float)thread->data_main->img_height;
	i = -1;
	while (++i < THREAD_NR)
	{
		y = -1 + (float)i * index_offset;
		y_fractal = -1;
		while (++y < (float)(i + 1) * index_offset)
		{
			++y_fractal;
			x = -1;
			while (++x < thread->data_main->img_width)
			{
				t = thread[i].fractal->p_number[x + y_fractal * thread->data_main->img_width];

				color_hsv.h = t % 256;
	    		color_hsv.s = 255;
	    		color_hsv.v = 255 * (t < thread[i].fractal->max_iterations);

				image->p_pixel[x + y * thread->data_main->img_width] = hsv_to_hexa(color_hsv);
			}
		}
	}
}

int		render(t_data *data)
{
	if (data->is_render_required == 1)
	{
		image_clear(data->image, 1);
		render_fractal(data->thread, data->image);
		mlx_put_image_to_window(data->mlx, data->win, data->image->p_image, (data->data_main->win_width - data->data_main->img_width) / 2, 0);
		//mlx_put_image_to_window(data->mlx, data->win, data->image_s->p_image, 0, 0);
		//mlx_string_put(data->mlx, data->win, 15, 15, 0x30ffffff, "Mandelbrot");
		//mlx_string_put(data->mlx, data->win, 15, 15, 0x30ffffff, "Julia");
		//mlx_string_put(data->mlx, data->win, 15, 35, 0x30ffffff, ft_itoa(data->fractal->zoom));
		data->is_render_required = 0;
	}
	return (0);
}
